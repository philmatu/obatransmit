//
//  AppDelegate.h
//  OBATransmit
//
//  Created by Philip Matuskiewicz on 5/15/13.
//  Copyright (c) 2013 Philip Matuskiewicz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
