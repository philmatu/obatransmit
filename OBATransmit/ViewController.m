//
//  ViewController.m
//  OBATransmit
//
//  Created by Philip Matuskiewicz on 5/15/13.
//  Copyright (c) 2013 Philip Matuskiewicz. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

//make interface variables from uibuilder available for use in this implementation
@synthesize outputTextBox;
@synthesize vehicleid;
@synthesize dsc;
@synthesize op;
@synthesize route;
@synthesize run;
@synthesize uri;
@synthesize delay;

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    //initialize _ignitionState and _requestid values to 0 on application load
    _ignitionState = 1;
    _requestid = 0;
    
    //heading and location initialization
    CLLocationManager* lm = [[CLLocationManager alloc] init];
    locman = lm;
    locman.delegate = self;
    locman.headingFilter = 2;
    [locman startUpdatingHeading];
    [locman startUpdatingLocation];
    
    direction = [@(locman.heading.trueHeading) stringValue];
    latitude = [@(locman.location.coordinate.latitude) stringValue];
    longitude = [@(locman.location.coordinate.longitude) stringValue];
    speed = [@(locman.location.speed) stringValue];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//heading detection
- (void) locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    CGFloat h = newHeading.trueHeading;
    direction = [@(h) stringValue];
    
}

//location detection
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation
{

    latitude = [@(newLocation.coordinate.latitude) stringValue];
    longitude = [@(newLocation.coordinate.longitude) stringValue];
    speed = [@(newLocation.speed) stringValue];
    
}

//implement actions on UIBuilder

- (IBAction) ignitionStateChange:(UISwitch *) sender
{
    //if the ignitionState switch changes, update the variable
    if(sender.on){
        _ignitionState = 1;
    }else{
        _ignitionState = 0;
    }
}

- (IBAction) enableChange:(UISwitch *) sender
{
    if(sender.on){

        if(!IS_AN_INTEGER(vehicleid.text)){
            [self alertUser:@"Bus # is not an Integer!"];
            [sender setOn:NO];
            return;
        }
        if(!IS_AN_INTEGER(dsc.text)){
            [self alertUser:@"DSC Code is not an Integer!"];
            [sender setOn:NO];
            return;
        }
        if(!IS_AN_INTEGER(op.text)){
            [self alertUser:@"Operator ID is not an Integer!"];
            [sender setOn:NO];
            return;
        }
        if(!IS_AN_INTEGER(route.text)){
            [self alertUser:@"Route ID is not an Integer!"];
            [sender setOn:NO];
            return;
        }
        if(!IS_AN_INTEGER(run.text)){
            [self alertUser:@"Run ID is not an Integer!"];
            [sender setOn:NO];
            return;
        }
        
        //TODO Validate URI possibly... here
        
        [self changeEditTextFieldsState:NO];
        
        int timerv = [delay.text intValue];
        
        [self startTimer:timerv];
        
        
    }else{
        [self changeEditTextFieldsState:YES];
        [self stopTimer];
    }
         
}

- (IBAction) delayChange:(UISlider *) sender
{
    if(_timer != nil){
        return;
    }
    
    NSString *myString = [NSString stringWithFormat:@"%2.0f", sender.value];//ensures 2 digit integer, no decimal
    delay.text = myString;
}

//on loss of focus of any textfield or textview object, make the keyboard go away
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void) alertUser:(NSString*)themessage
{
    UIAlertView *alert = [[UIAlertView alloc]   initWithTitle:@"Error"
                                                message:themessage
                                                delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
    [alert show];
}

- (void) changeEditTextFieldsState:(BOOL)newstate
{
    vehicleid.enabled = newstate;
    dsc.enabled = newstate;
    op.enabled = newstate;
    route.enabled = newstate;
    run.enabled = newstate;
    uri.enabled = newstate;
    
}

#define ISO_TIMEZONE_OFFSET_FORMAT @"%03d:%02d"
- (NSString *) strFromISO8601:(NSDate *) date
{
    
    
    static NSDateFormatter* sISO8601 = nil;
    
    if (!sISO8601) {
        sISO8601 = [[NSDateFormatter alloc] init];
        
        NSTimeZone *timeZone = [NSTimeZone localTimeZone];
        int offset = [timeZone secondsFromGMT];
        
        NSMutableString *strFormat = [NSMutableString stringWithString:@"yyyy-MM-dd'T'HH:mm:ss.0"];
        offset /= 60; //bring down to minutes

        [strFormat appendFormat:ISO_TIMEZONE_OFFSET_FORMAT, offset / 60, offset % 60];
        
        [sISO8601 setTimeStyle:NSDateFormatterFullStyle];
        [sISO8601 setDateFormat:strFormat];
    }
    return[sISO8601 stringFromDate:date];
    
}

- (void) startTimer:(double)ticktime
{
    if (_timer == nil)
    {
        _timer = [NSTimer                         scheduledTimerWithTimeInterval:ticktime
                                                  target:self
                                                  selector:@selector(timerTick)
                                                  userInfo:nil
                                                  repeats:YES];
    }
    //do first tick now
    [self timerTick];
}

- (void) stopTimer
{
    if (_timer != nil)
    {
        [_timer invalidate];
        _timer = nil;
    }
}


- (void) timerTick
{
    outputTextBox.text = @"";
    
    NSDate *currentDate = [NSDate date];
    NSString *rfc8601date = [self strFromISO8601:currentDate];
    
    NSString *rep = [self formulateCCLocationReport:rfc8601date];
    
    NSString *res = [self postToServer:rep];
    
    outputTextBox.text = [outputTextBox.text stringByAppendingString:@"["];
    outputTextBox.text = [outputTextBox.text stringByAppendingString:res];
    outputTextBox.text = [outputTextBox.text stringByAppendingString:@"]:["];
    outputTextBox.text = [outputTextBox.text stringByAppendingString:rfc8601date];
    outputTextBox.text = [outputTextBox.text stringByAppendingString:@"]:"];
    outputTextBox.text = [outputTextBox.text stringByAppendingString:rep];
    outputTextBox.text = [outputTextBox.text stringByAppendingString:@"\n"];
}

- (NSString *) postToServer:(NSString*)message
{
    //trim whitespace entirely
    NSString *doubleTrimmed = [message stringByReplacingOccurrencesOfString:@"[ \r\n\t]+" withString:@" " options:NSRegularExpressionSearch range:NSMakeRange(0, message.length)];
    NSString *secondString = [doubleTrimmed stringByReplacingOccurrencesOfString:@"^[ \r\n\t]+(.*)[ \r\n\t]+$" withString:@"$1" options:NSRegularExpressionSearch range:NSMakeRange(0, doubleTrimmed.length)];
    NSString *jsonRequest = [secondString stringByReplacingOccurrencesOfString:@" " withString:@""];

    NSURL *url = [NSURL URLWithString:uri.text];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.timeoutInterval = 10;
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: requestData];
    
    [NSURLConnection connectionWithRequest:request delegate:self];
  
    NSError *error;
    NSURLResponse *response;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    int responseStatusCode = [httpResponse statusCode];
    return[@(responseStatusCode) stringValue];
     
}

- (NSString *)formulateCCLocationReport:(NSString*)rfc8601date
{
    _requestid++;
    return [NSString stringWithFormat:
     @"{\"CcLocationReport\":\n\
       {\n\
            \"request-id\":%@,\n\
            \"vehicle\":\n\
                {\n\
                 \"vehicle-id\":%@,\n\
                 \"agency-id\":2008,\n\
                 \"agencydesignator\":\"MTA NYCT\"\n\
                },\n\
            \"status-info\":0,\n\
            \"time-reported\":\n\
                \"%@\",\n\
            \"latitude\":%@,\n\
            \"longitude\":%@,\n\
            \"direction\":\n\
                {\n\
                    \"deg\":%@\n\
                },\n\
            \"speed\":%@,\n\
            \"data-quality\":\n\
                {\n\
                    \"qualitative-indicator\":\"4\"\n\
                },\n\
            \"manufacturer-data\":\"iPhoneDEV\",\n\
            \"operatorID\":\n\
                {\n\
                    \"operator-id\":0,\n\
                    \"designator\":\"%@\"\n\
                },\n\
            \"runID\":\n\
                {\n\
                    \"run-id\":0,\n\
                    \"designator\":\"%@\"\n\
                },\n\
            \"destSignCode\":%@,\n\
            \"routeID\":\n\
                {\n\
                    \"route-id\":0,\n\
                    \"route-designator\":\"%@\"\n\
                },\n\
            \"localCcLocationReport\":\n\
                {\n\
                    \"NMEA\":\n\
                    {\n\
                        \"sentence\":\n\
                            [\n\
                                \"\",\"\"\
                            ]\n\
                    },\n\
                    \"vehiclePowerState\":%@\n\
                }\n\
        }\n\
    }\n\
    "
                     
                     ,//end literal part
                        [@(_requestid) stringValue],//request-id
                        vehicleid.text,//vehicle-id
                        rfc8601date,//time-reported
                        [latitude stringByReplacingOccurrencesOfString:@"." withString:@""],
                        [longitude stringByReplacingOccurrencesOfString:@"." withString:@""],
                        direction,
                        speed,
                        op.text,//operator id
                        run.text,//run id
                        dsc.text,//destination sign code
                        route.text,//route number
                        [@(_ignitionState) stringValue]
                     ];
    
    
    /*
     
     
     
     \"emergencyCodes\":\n\
     {\n\
     },\n\
    */
}


@end
