//
//  ViewController.h
//  OBATransmit
//
//  Created by Philip Matuskiewicz on 5/15/13.
//  Copyright (c) 2013 Philip Matuskiewicz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreLocation/CoreLocation.h"

#define REGEX_FOR_INTEGERS  @"^([+-]?)(?:|0|[1-9]\\d*)?$"
#define IS_AN_INTEGER(string) [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", REGEX_FOR_INTEGERS] evaluateWithObject:string]

@interface ViewController : UIViewController <CLLocationManagerDelegate>

{
    
    //defined variables of the Interface Builder
    IBOutlet UITextView *outputTextBox;
    IBOutlet UITextField *vehicleid;
    IBOutlet UITextField *dsc;
    IBOutlet UITextField *op;
    IBOutlet UITextField *route;
    IBOutlet UITextField *run;
    IBOutlet UITextField *uri;
    IBOutlet UILabel *delay;
    
    //other variables in use by the application
    NSTimer *_timer;
    int _requestid;
    int _ignitionState;
    
    //location variables
    CLLocationManager *locman;
    NSString *direction;
    NSString *latitude;
    NSString *longitude;
    NSString *speed;
}

@property (nonatomic, retain) IBOutlet UITextView *outputTextBox;
@property (nonatomic, retain) IBOutlet UITextField *vehicleid;
@property (nonatomic, retain) IBOutlet UITextField *dsc;
@property (nonatomic, retain) IBOutlet UITextField *op;
@property (nonatomic, retain) IBOutlet UITextField *route;
@property (nonatomic, retain) IBOutlet UITextField *run;
@property (nonatomic, retain) IBOutlet UITextField *uri;
@property (nonatomic, retain) IBOutlet UILabel *delay;

//location functions
- (void) locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading*)newHeading;
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation;

//actions tied to the UIBuilder
- (IBAction) ignitionStateChange:(UISwitch *) sender;
- (IBAction) enableChange:(UISwitch *) sender;
- (IBAction) delayChange:(UISlider *) sender;

//closes keyboard on loss of focus
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;

- (void) alertUser:(NSString*)themessage;
- (NSString *) strFromISO8601:(NSDate *) date;
- (void) changeEditTextFieldsState:(BOOL)newstate;
- (void) startTimer:(double)ticktime;
- (void) stopTimer;
- (void) timerTick;
- (NSString *) postToServer:(NSString*)message;

- (NSString *)formulateCCLocationReport:(NSString*)rfc8601date;

@end

